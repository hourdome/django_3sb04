มีการใช้ : Fomantic-ui
https://fomantic-ui.com/


วิธีลง Fomantic : https://fomantic-ui.com/introduction/getting-started.html

1. สร้าง package.json

    `npm init`
2. ลง Fomantic-ui

    `npm install fomantic-ui`
3. ลงเพื่อรับ features ใหม่กับแก้ bug ต่างๆ

    `npm install fomantic-ui@nightly`

หรือใช้เป็น jsDelivr


`<script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>`

`<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.css">`

`<script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.js"></script>`
